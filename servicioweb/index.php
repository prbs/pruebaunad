<?php 
	session_start();
	unset($_SESSION['usuario']);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
</head>
<body>
<div class="w3-container" style="width: 550px; margin: auto;">
	<div class="w3-container w3-teal">
		<h2>Sistema de Autenticación</h2>
	</div>

	<form class="w3-container" action="ctrl_acceso.php" method="post">
		<p>
			<label class="w3-label">Usuario (Nombre en Tabla1 - Ejm. Nombre 31)</label>
			<input class="w3-input w3-border " type="text" name="usuario">
		</p>
		<p>
			<label class="w3-label">Password (Cédula en Tabla1 - Ejm. 10000031)</label>
			<input class="w3-input w3-border" type="text" name="pas">
		</p>
		<p>
			<input type="hidden" name="entrar" value="entrar">
			<button class="w3-btn w3-green">Aceptar</button>
			<?php
				if (isset($_GET['error'])) echo '<label class="w3-label">Datos errados, intente de nuevo.</label>';
			?>
		</p>
	</form>
	<footer>
		<div class="w3-container w3-teal">
			<h4>Oscar Abaunza - 2022</h4>
		</div>
	</footer>
</div>
</body>
</html>