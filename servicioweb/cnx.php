<?php 
	class Db{
		private static $conexion=null;
		private function __construct(){}

		public static function conectar(){
			$pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
			self::$conexion=new PDO('mysql:host=localhost;dbname=baseuno','root','root',$pdo_options);
			return self::$conexion;
		}
	}
	
	class Conexion extends PDO {
		private $hostBd = 'localhost';
		private $nombreBd = 'baseuno';
		private $usuarioBd = 'root';
		private $passwordBd = 'root';
		
		public function __construct()
		{
			try{
				parent::__construct('mysql:host=' . $this->hostBd . ';dbname=' . $this->nombreBd . ';charset=utf8', $this->usuarioBd, $this->passwordBd, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
				
				} catch(PDOException $e){
				echo 'Error: ' . $e->getMessage();
				exit;
			}
		}
	}
	
	$cnx = @mysql_connect('127.0.0.1','root','root');
	if (!$cnx) {
		die('Error de conexión: ' . mysql_error());
	}
	mysql_select_db('baseuno', $cnx);

?>