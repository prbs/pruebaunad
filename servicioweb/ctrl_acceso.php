<?php 
	require_once('usuario.php');
	require_once('crud_acceso.php');
	require_once('cnx.php');

	//inicio de sesion
	session_start();

	$usuario=new Usuario();
	$crud=new CrudUsuario();
	if (isset($_POST['entrar'])) {
		$usuario=$crud->obtenerUsuario($_POST['usuario'],$_POST['pas']);
		if ($usuario->getId()!=NULL) {
			$_SESSION['usuario']=$usuario;
			header('Location: cliente.php');
		}else{
			//header('Location: error.php?mensaje=Usuario o clave incorrectos');
			header('Location: index.php?error=1');
		}
	}elseif(isset($_POST['salir'])){
		header('Location: index.php');
		unset($_SESSION['usuario']); //destruye la sesión
	}
?>