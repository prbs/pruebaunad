<?php
	include '../cnx.php';
	
	$pdo = new Conexion();
	
	//Listar registros y consultar registro
	if($_SERVER['REQUEST_METHOD'] == 'GET'){
		if(isset($_GET['cedula']))
		{
			$sql = $pdo->prepare("SELECT * FROM tabla1 WHERE cedula=:cedula");
			$sql->bindValue(':cedula', $_GET['cedula']);
			$sql->execute();
			$sql->setFetchMode(PDO::FETCH_ASSOC);
			$resultado = $sql->fetchAll();
			header("HTTP/1.1 200 hay datos");
			//echo json_encode($resultado);
			
			if($resultado[0]['cedula']!=NULL){
				$sql = "INSERT INTO tabla2 (nombre, cedula) VALUES(:nombre, :cedula)";
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(':nombre', $resultado[0]['nombre']);
				$stmt->bindValue(':cedula', $resultado[0]['cedula']);
				$stmt->execute();
				$idPost = $pdo->lastInsertId(); 
				if($idPost)
				{
					header("HTTP/1.1 200 Ok");
					//echo json_encode($idPost);
					header('Location: ../cliente.php?id='.$idPost.'&cedula='.$_GET['cedula']);
					exit;
				}
			} else {
				header("HTTP/1.1 400 Bad Request");
				header('Location: ../cliente.php?id=0&cedula='.$_GET['cedula']);
				exit;
			}
			exit;				
		}
		if(isset($_GET['id']))
		{
			$sql = $pdo->prepare("SELECT * FROM tabla1 WHERE id=:id");
			$sql->bindValue(':id', $_GET['id']);
			$sql->execute();
			$sql->setFetchMode(PDO::FETCH_ASSOC);
			header("HTTP/1.1 200 hay datos");
			echo json_encode($sql->fetchAll());
			exit;				
			
			/*} else {
			
			$sql = $pdo->prepare("SELECT * FROM tabla1");
			$sql->execute();
			$sql->setFetchMode(PDO::FETCH_ASSOC);
			header("HTTP/1.1 200 hay datos");
			echo json_encode($sql->fetchAll());
			exit;*/	
		}
	}
	
	//Insertar registro
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$sql = "INSERT INTO tabla1 (nombre, cedula) VALUES(:nombre, :cedula)";
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(':nombre', $_POST['nombre']);
		$stmt->bindValue(':telefono', $_POST['cedula']);
		$stmt->execute();
		$idPost = $pdo->lastInsertId(); 
		if($idPost)
		{
			header("HTTP/1.1 200 Ok");
			echo json_encode($idPost);
			exit;
		}
	}
	
	//Actualizar registro
	if($_SERVER['REQUEST_METHOD'] == 'PUT')
	{		
		$sql = "UPDATE tabla1 SET nombre=:nombre, cedula=:cedula WHERE id=:id";
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(':nombre', $_GET['nombre']);
		$stmt->bindValue(':cedula', $_GET['cedula']);
		$stmt->bindValue(':id', $_GET['id']);
		$stmt->execute();
		header("HTTP/1.1 200 Ok");
		exit;
	}
	
	//Eliminar registro
	if($_SERVER['REQUEST_METHOD'] == 'DELETE')
	{
		$sql = "DELETE FROM tabla1 WHERE id=:id";
		$stmt = $pdo->prepare($sql);
		$stmt->bindValue(':id', $_GET['id']);
		$stmt->execute();
		header("HTTP/1.1 200 Ok");
		exit;
	}
	
	//Si no corresponde a ninguna opción anterior
	header("HTTP/1.1 400 Bad Request");
?>