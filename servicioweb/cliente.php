<?php
	session_start();
	if (!isset($_SESSION['usuario'])) {
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Formulario demostración de datos Mysql - Oscar Abaunza</title>
	<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/themes/color.css">
	<link rel="stylesheet" type="text/css" href="https://www.jeasyui.com/easyui/demo/demo.css">
	<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
	<script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="https://www.jeasyui.com/easyui/jquery.easyui.min.js"></script>
</head>
<body>
	<div class="w3-container w3-teal w3-center">
		<h1>Aplicación de Web Service</h1>
	</div>
	<p></p>
	<form class="w3-container" action="ws" method="get">
		<label>Ingrese el número de cédula a consultar:</label>
		<input type="txt" name="cedula" value="">
		<button class="w3-btn w3-green">Consultar</button>
	<?php
		if (isset($_GET['id'])){
			if ($_GET['id']>0){
				echo '<label>*Se encontró la cédula '.$_GET['cedula'].' en la tabla 1 y se agregó el registro en tabla2.</label>';
			} else {
				echo '<label>*No se encontró la cédula '.$_GET['cedula'].' en la tabla 1.</label>';
			}
		}
	?>

	</form>
	<h2>Listado de datos en Tabla2</h2>
	<div style="margin-bottom:10px">
	    </div>
	<table id="dg" title="Datos en Tabla2" class="easyui-datagrid" style="width:700px;height:250px"
			url="obtener.php?tb=tabla2"
			toolbar="#toolbar" pagination="true"
			rownumbers="true" fitColumns="true" singleSelect="true">
		<thead>
			<tr>
				<th field="nombre" width="50">Nombre</th>
				<th field="cedula" width="50">Cédula</th>
			</tr>
		</thead>
	</table>
	
	<div id="dlg" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
		<form id="fm" method="post" novalidate style="margin:0;padding:20px 50px">
			<h3>Información del registro</h3>
			<div style="margin-bottom:10px">
				<input name="nombre" class="easyui-textbox" required="true" label="Nombre:" style="width:100%">
			</div>
			<div style="margin-bottom:10px">
				<input name="cedula" class="easyui-textbox" required="true" label="Cédula:" style="width:100%">
			</div>
		</form>
	</div>
	
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="grabar()" style="width:90px">Save</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
	</div>
	<p></p>
	<form class="w3-container" action="ctrl_acceso.php" method="post">
		<input type="hidden" name="salir" value="salir">
		<button class="w3-btn w3-green">Salir</button>
	</form>

	<script type="text/javascript">
		var url;
		function cambiar(){
			$('#dg').datagrid('load', 'obtener.php?tb='+$('#s-tabla').val());
			$('#dg').datagrid('getPanel').panel('setTitle', 'Datos en '+$('#s-tabla').val());
		}
	</script>
</body>
</html>