CREATE DATABASE IF NOT EXISTS baseuno CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE DATABASE IF NOT EXISTS basedos CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;


USE baseuno;

DELIMITER //
CREATE PROCEDURE tablasydatos()
BEGIN
	DECLARE contador INT UNSIGNED DEFAULT 1;
	DECLARE nom VARCHAR(20);
	DECLARE ced INT(10);
	DECLARE fin BOOLEAN DEFAULT FALSE;
	DECLARE cur1 CURSOR FOR SELECT nombre, cedula FROM tabla1 LIMIT 0,50;  
	DECLARE cur2 CURSOR FOR SELECT cedula FROM tabla1 LIMIT 0,20;  
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin = TRUE;
	/* Verifica si existe tabla1 */
	SELECT COUNT(1) INTO @existe FROM information_schema.tables WHERE table_schema=DATABASE() AND TABLE_NAME='tabla1';
	/* SI NO existe, crea tabla1 */
	IF @existe = 0 THEN
		CREATE TABLE tabla1 (id INT(10) NOT NULL AUTO_INCREMENT, nombre VARCHAR(20), cedula INT(10) DEFAULT 0, PRIMARY KEY (id));
	ELSE
		TRUNCATE TABLE tabla2;
	END IF;
	/* Verifica si existe tabla2 */
	SELECT COUNT(1) INTO @existe FROM information_schema.tables WHERE table_schema=DATABASE() AND TABLE_NAME='tabla2';
	/* Si NO existe, crea tabla2 */
	IF @existe = 0 THEN
		CREATE TABLE tabla2 (id INT(10) NOT NULL AUTO_INCREMENT, nombre VARCHAR(20), cedula INT(10) DEFAULT 0, PRIMARY KEY (id));
	ELSE
		TRUNCATE TABLE tabla2;
	END IF;
	/* Inserta 100 registros en tabla1 y tabla2 */
	WHILE contador <= 100 DO
		INSERT INTO tabla1 (nombre, cedula) values (CONCAT('Nombre ',contador),10000000+contador);
		INSERT INTO tabla2 (nombre, cedula) values (CONCAT('Nombre ',contador),20000000+contador);
		SET contador=contador+1;
	END WHILE;
	#INSERT INTO tabla2 (nombre, cedula) SELECT nombre, cedula FROM tabla1;
	/* Ciclo con Cursor agrega 50 registros de tabla1 a tabla2 */
	OPEN cur1;
	FETCH cur1 INTO nom, ced;
	WHILE (NOT fin) DO
		INSERT INTO tabla2 (nombre, cedula) values (nom, ced);
		FETCH cur1 INTO nom, ced;
	END WHILE;
	CLOSE cur1;
	/* Ciclo con Cursor elimina 20 registros de tabla1 */
	SET fin = FALSE;
	OPEN cur2;
	FETCH cur2 INTO ced;
	WHILE (NOT fin) DO
		DELETE FROM tabla1 WHERE cedula=ced;
		FETCH cur2 INTO ced;
	END WHILE;
	CLOSE cur2;
END //
DELIMITER ;


CALL tablasydatos();